package escenarios;

public  class Map {
	
	private int x;
	private int y;
	
	/**
	 * Permite solo crear un escenario cuando se tiene un tama�o especifico, No se puede alterar el tama�o una vez creado
	 * @param x
	 * @param y
	 */
	public Map(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	

}
