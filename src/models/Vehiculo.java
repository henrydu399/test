package models;

import escenarios.Map;

public class Vehiculo {
	
	private static final int size = 1; //tama�o ocupado en matrix
	private String color;
	private int velocidad; //velozidad milisegundos
	private int x = 0; 
	private int y;
	
	
	/**
	 * Segun las reglas el vehiculo siempre incia en una posicion.
	 * Debe existir un mapa para obtener la pocision de inicio
	 */
	public Vehiculo (Map map) {
		this.x = 1;
		this.y = 1;
	}



	
	
	public int getY() {
		return y;
	}





	public void setY(int y) {
		this.y = y;
	}





	public int getX() {
		return x;
	}



	public void setX(int x) {
		this.x = x;
	}



	public void CambioPosicion(int x , int y) {
		this.x = x;
		this.y = y;
	}
	
	
	
	

}
