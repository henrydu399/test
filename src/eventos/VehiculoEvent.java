package eventos;

import escenarios.Map;
import models.Vehiculo;

public class VehiculoEvent implements VehiculoEventImp {

	private int Desplazamiento;
	private String Direccion;
	private static final String[] DIRECCIONES = { "N", "S", "O", "E" };
	private Vehiculo vehiculo;
	private Map map;

	/**
	 * Contrustor que me permite que el vehiculo pueda realizar un movimiento,
	 * Siempre que exista un vehiculo y map como minimo
	 * 
	 * @param vehiculo
	 * @param map
	 */
	public VehiculoEvent(Vehiculo vehiculo, Map map) {
		this.vehiculo = vehiculo;
		this.map = map;
	}

	/**
	 * Metodo para mover el vehiculo sobre escenario
	 */

	@Override
	public boolean move(String mov) {
		boolean out = true;
		
		if(!ValidarFormatMov(mov))
			out = false;
		if(!ValidarMov())
			out = false;
		
		return out;

	}

	/**
	 * Metodo que permite validar el movimiento sobre el mapa
	 * 
	 * @return
	 */
	public boolean ValidarMov() {
		boolean out = true;
		int MaxAvance;
		switch (this.Direccion) {

		case "N": // tomado "N" como arriba
			MaxAvance = map.getY() - vehiculo.getY();
			if (vehiculo.getY() != map.getY()) {
				if (this.Desplazamiento  <= MaxAvance ) {
					vehiculo.setY(vehiculo.getY() + this.Desplazamiento);
				} else {
					System.out.println("El desplazamiento excede el limite del mapa");
					out = false;
				}
			}else {
				System.out.println("El desplazamiento excede el limite del mapa");
			}

			break;

		case "S": // tomado "S" como abajo

			if (vehiculo.getY() != 1) {
				MaxAvance = ((vehiculo.getY() - map.getY()) + map.getY());

				if (this.Desplazamiento <= MaxAvance) {
					vehiculo.setY(vehiculo.getY() - this.Desplazamiento);
				} else {
					System.out.println("El desplazamiento excede el limite del mapa");
					out = false;
				}

			} else {
				System.out.println("El desplazamiento excede el limite del mapa");
				out = false;
			}
			break;

		case "E": // tomado "E" como lado izquierda

			if (vehiculo.getX() != 1) {
				MaxAvance = ((vehiculo.getX() - map.getX()) + map.getX());
				if (this.Desplazamiento <= MaxAvance) {
					vehiculo.setX(vehiculo.getX() - this.Desplazamiento);
				} else {
					System.out.println("El desplazamiento excede el limite del mapa");
					out = false;
				}
			} else {
				System.out.println("El desplazamiento excede el limite del mapa");
				out = false;
			}

			break;

		case "O": // tomado "O" como lado derecho

			if (vehiculo.getX() != map.getX()) {
				MaxAvance = ((vehiculo.getX() - map.getX()) * -1);
				if (this.Desplazamiento <= MaxAvance) {
					vehiculo.setX(vehiculo.getX() + this.Desplazamiento);
				} else {
					System.out.println("El desplazamiento excede el limite del mapa");
					out = false;
				}
			} else {
				System.out.println("El desplazamiento excede el limite del mapa");
				out = false;
			}

			break;

		}

		return out;
	}

	/***
	 * Metodo que permite comprobar movimientos validos
	 * 
	 * @param mov
	 * @return boolean true : correcto | false : no permitido
	 */

	private boolean ValidarFormatMov(String mov) {
		boolean out = true;
		String[] items = mov.split(",");

		if (items.length != 2) {
			System.out.println("Formato movimiento invalido");
			return false;
		}

		try {
			if (!items[0].equals("")) {

				if (Integer.parseInt(items[0]) > 0) {
					this.Desplazamiento = Integer.parseInt(items[0]);
				} else {
					System.out.println("Formato movimiento invalido");
					return false;

				}

			}
			if (!items[1].equals("")) {
				boolean existe = false;
				// verificamos si la direccion es correcta
				for (String temp : DIRECCIONES) {
					if (items[1].equals(temp)) {
						existe = true;
					}
				}

				if (existe) {
					this.Direccion = items[1];
				} else {
					System.out.println("Formato direccion invalido");
					return false;
				}

			}
		} catch (NumberFormatException e) {
			System.out.println("Pakete : eventos | Class :vehiculoEvent | error : Convert Formato movimiento invalido");
			return false;
		}

		return out;

	}

}
