package main;

import java.util.Scanner;

import escenarios.Map;
import eventos.VehiculoEvent;
import eventos.VehiculoEventImp;
import models.Vehiculo;

public class main {

	public static Scanner entradaEscaner;
	public static Vehiculo vehiculo;
	public static Map map;
	public static String entradaTeclado;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		entradaEscaner = new Scanner(System.in); // Creaci�n de un objeto Scanner
		menu();
	}

	public static boolean menu() {

		boolean out = true;

		System.out.println(" Bienvenido  Game Test ");

		System.out.println(" 1.Crear Escenario");
		System.out.println(" 2.Crear vehiculo");
		System.out.println(" 3. jugar");
		System.out.println(" 4.Salir");

		entradaTeclado = entradaEscaner.nextLine(); // Invocamos un m�todo sobre un objeto Scanner}
		int opc = Integer.parseInt(entradaTeclado);
		try {

			switch (opc) {
			case 1:
				crearEscenario();
				break;

			case 2:
				crearVehiculo();
				break;

			case 3:
				jugar();
				break;

			case 4:
				return true;

			}

		} catch (NumberFormatException e) {
			System.out.println("Pakete : main | Class :main | error : Convert Formato opcion invalido");

		}

		return out;

	}

	private static void jugar() {
		
		if(map != null && vehiculo != null ) {
			System.out.println("Se debe crear un escenario primero ;");
			

		VehiculoEvent VehImpl = new VehiculoEvent(vehiculo, map);
		String[] Movimientos;
		
		
		while (true) {
			Render();
			System.out.println(" Digite su movimiento, Sin son varios separelos por ;");
			entradaTeclado = entradaEscaner.nextLine();
			// Guardamos todos los movimientos en array separados x ";"
			Movimientos = entradaTeclado.split(";");
			// ejecutamos los movimientos permitidos de todos.
			for (int a = 0; a < Movimientos.length; a++) {
				if (VehImpl.move(Movimientos[a]) ==  false) {
					break;
				}
					
			}

		}

		}else {
			System.out.println(" Se debe crear un ESCENARIO Y VEHICULO para poder jugar ;");
			menu();
			}
		// TODO Auto-generated method stub

	}

	private static void crearVehiculo() {
		vehiculo = new Vehiculo(map);
		System.out.println(" Se ha creado el vehiculo con exito");
		menu();
	}

	private static void crearEscenario() {
		int x, y;

		try {

			System.out.println("Ingrese tama�o en x ");
			entradaTeclado = entradaEscaner.nextLine();
			x = Integer.parseInt(entradaTeclado);
			System.out.println("Ingrese tama�o en y ");
			entradaTeclado = entradaEscaner.nextLine();
			y = Integer.parseInt(entradaTeclado);

			if (x != 0 && y != 0) {
				map = new Map(x, y);
				System.out.println(" Se ha creado el escenario con exito");
			} else {
				System.out.println(" El tama�o del escenario es invalido");
			}

			menu();

		} catch (NumberFormatException e) {
			System.out.println("Pakete : main | Class :main | error : Convert Formato tama�o invalido");

		}

	}

	/**
	 * Simula la renderizacion de la pantalla
	 */
	private static void Render() {

		for (int a = map.getY(); a >= 1; a--) {
			for (int b = 1; b <= map.getX(); b++) {

				if (vehiculo.getX() == b && vehiculo.getY() == a) {
					System.out.print(" |X| ");
				} else {
					System.out.print(" | | ");
				}
			}
			System.out.println("");
		}
		
		System.out.println("");
	}

}
